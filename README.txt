The project is broken into the main machine learning library (mllib.py), the UCI downloader (downloader.py), and the
scripts for the five UCI datasets (bcw.py, glass.py, iris.py, soybean.py, and voting.py). The scripts run tests
on each respective dataset, writing to the project5.log file and stdout.

(1) Install prerequisite libraries:
pip install -r requirements.txt

(2) Download datasets:
python download.py

(3) Run tests on each dataset:
python bcw.py
python glass.py
python iris.py
python soybean.py
python voting.py