import urllib.request as request
import os
import re

repository_url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/{dataset}/{file}'
data_dir = 'data'
datasets = [
    'breast-cancer-wisconsin',
    'glass',
    'iris',
    'soybean',
    'voting-records'
]

def uci_downloader(sources):
    for s in sources:
        print('downloading from source "' + s + '"')
        drop = data_dir + '/' + s
        if not os.path.exists(drop):
            os.makedirs(drop)
        index = request.urlopen(repository_url.format(dataset=s,file=''))
        files = re.findall('<a href="([\w\.-]+)">', index.read().decode())
        count = len(files)

        for i,f in enumerate(files):
            url = repository_url.format(dataset=s,file=f)
            print('[{}/{}] '.format(i+1,count) + url)
            request.urlretrieve(url, '{}/{}'.format(drop,f))

uci_downloader(datasets)