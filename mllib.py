import numpy as np
import logging

logging.basicConfig(filename='project5.log',level=logging.DEBUG)

class MlInstance:
    """
        Hosts a singular data instance

    """
    def __init__(self,feats,cls):
        self.cls = cls
        self.features = np.array(feats)

    def __str__(self):
        return 'class="{}",features={}'.format(self.cls,self.features)

class MlDataset:
    """
        Hosts a base singular dataset
    """
    def __init__(self,src):
        self.src = src
        self.instances = []

    def __str__(self):
        return 'src="{}",classes="{}",instances={}'.format(self.src,self.classes,len(self.instances))

    def load(self,df,start_index=0,end_index=-1,cls_index=-1,fields=None):
        self.instances = [ MlInstance(list(row[1][start_index:end_index+1]),row[1].iloc[cls_index]) for row in df.iterrows() ]
        self.fields = [ i for i in range(len(df.iloc[0][start_index:end_index+1])) ] if not fields else fields
        logging.info(f'loaded dataset {self.src}')

    def clean(self,dtypes,arr_dtype=object,g_feat=None,g_cls=None):
        self.dtypes = dtypes
        for instance in self.instances:
            instance.cls = g_cls(instance.cls) if g_cls else instance.cls
            new_features = []
            for i,feature in enumerate(instance.features):
                new_features.append(dtypes[i](g_feat(feature)) if g_feat else feature)
            instance.features = np.array(new_features,dtype=arr_dtype)
        self.classes = np.unique([ inst.cls for inst in self.instances ])
        logging.info(f'cleaned dataset {self.src}')
        
class ClassDataset(MlDataset):
    """
        Hosts a base class-based dataset
    """
    def partition(self,val):
        if val > len(self.instances):
            print('error: invalid bin count')
            return []
        class_bins = { cls : [] for cls in self.classes } # instantiate class bins
        for inst in self.instances:
            class_bins[inst.cls].append(inst) # place each instance in its own class bin
        validation_bins = [ [] for i in range(val) ] # instantiate validation bins
        for cls in class_bins: # for each class, iterate through all of its instances
            for inst in class_bins[cls]:
                cls_len = len(class_bins[cls])
                rand_int = np.random.randint(cls_len)
                validation_bins[cls_len % val].append(class_bins[cls].pop(rand_int))
        return validation_bins

class RegressionDataset(MlDataset):
    """
        Hosts a base regressor-based dataset
        with continuous values as class values
    """
    def partition(self,val):
    # this partition will first bin based on regressed value,
    # then place those instances in validation bins at random
        if val > len(self.instances):
            print('error: invalid bin count')
            return []
        sorted_instances = sorted(self.instances,key=lambda x: x.cls)
        partitioned_instances = np.array_split(np.array(sorted_instances),val)
        validation_bins = [ [] for i in range(val) ] # instantiate validation bins
        for part in partitioned_instances:
            for inst in part:
                part_len = len(part)
                rand_int = np.random.randint(part_len)
                validation_bins[part_len % val].append(part[rand_int])
                part = np.delete(part,rand_int)
        return validation_bins
    
class NaiveBayesModel:
     
    def __init__(self,name,multi={}):
        self.name = name
        self.probs = {}
        self.multi = multi

    def __str__(self):
        return 'NaiveBayesModel,name={},probs={}'.format(self.name,self.probs)
    
    def train(self,data):

        logging.info('training NB model')
        
        probs = { }
        
        self.multi_stats = self.create_one_hot(data)
        
        for d in data:
            
            if d.cls not in probs:
                probs[d.cls] = { 'vals': [ {} for f in d.features ], 'count' : 0}
            
            probs[d.cls]['count'] += 1
            
            for i,f in enumerate(d.features):
                
                if i in self.multi:
                    
                    if 'mean' not in probs[d.cls]['vals'][i]:
                        probs[d.cls]['vals'][i]['mean'] = self.multi_stats[d.cls][i]['mean']
                        probs[d.cls]['vals'][i]['std'] = self.multi_stats[d.cls][i]['std']
                        probs[d.cls]['vals'][i]['high'] = 0
                        probs[d.cls]['vals'][i]['low'] = 0
                        
                    if abs(probs[d.cls]['vals'][i]['mean'] - f) < probs[d.cls]['vals'][i]['std']:
                        probs[d.cls]['vals'][i]['low'] += 1
                    else:
                        probs[d.cls]['vals'][i]['high'] += 1
                                        
                else:
                    
                    if f not in probs[d.cls]['vals'][i]:
                        probs[d.cls]['vals'][i][f] = 1
                    else:
                        probs[d.cls]['vals'][i][f] += 1
                        
        self.total = len(data)
        self.probs = probs

        logging.info('completed training NB model:')
        logging.info(probs)

        return probs
    
    def test(self,data):

        total = len(data)
        correct = 0
        
        for d in data:
            
            potentials = {}
            
            for p in self.probs:
                
                cls_prob = self.probs[p]['count'] / self.total
                feat_prob = 1
                
                for i,f in enumerate(d.features):
                    if i not in self.multi:
                        if f in self.probs[p]['vals'][i]:
                            feat_prob = feat_prob * (self.probs[p]['vals'][i][f] / self.probs[p]['count'])
                        else:
                            feat_prob = feat_prob * 0.1
                
                    else:
                        if abs(self.probs[p]['vals'][i]['mean'] - f) < self.probs[p]['vals'][i]['std']:
                            feat_prob = feat_prob * (self.probs[p]['vals'][i]['low'] / self.probs[p]['count'])
                        else:
                            feat_prob = feat_prob * (self.probs[p]['vals'][i]['high'] / self.probs[p]['count'])
                            
                potentials[p] = cls_prob * feat_prob
            
            predicted = max(potentials, key=potentials.get)
            actual = d.cls
            if actual == predicted:
                correct += 1

        return correct / total
                        
    def create_one_hot(self,data):
        stats = { }
        for f in self.multi:
            
            for d in data:
                if d.cls not in stats:
                    stats[d.cls] = { }
                if f not in stats[d.cls]:
                    stats[d.cls][f] = {'vals':[]}
                stats[d.cls][f]['vals'].append(d.features[f])
            
        for cls in stats.keys():
            for f in stats[cls]:
                stats[cls][f]['mean'] = np.mean(stats[cls][f]['vals'])
                stats[cls][f]['std'] = np.std(stats[cls][f]['vals'])

        return stats
class LogisticRegressionModel():
     
    def __init__(self,name,classes,fields,multi={}):
        self.name = name
        self.probs = {}
        self.classes = classes
        self.fields = fields
        self.multi = multi

    def __str__(self):
        return 'LogisticRegressionModel,name={},probs={}'.format(self.name,self.probs)
    
    def train(self,data,rand_range=[-0.01,0.01]):
                
        W = { c : [ np.random.uniform(rand_range[0],rand_range[1]) for f in self.fields ] for c in self.classes }
        Y = { c : 0 for c in self.classes }
        r = lambda t,y: 1 if t == y else 0
        x = lambda x: x if x != 0 else 1
                
        eta = 0.001
        
        self.multi_stats = self.create_one_hot(data)
        
        for i in range(100): # should be while loop
            
            d_W = { c : [ 0 for f in self.fields ] for c in self.classes }

            for d in data:
                
                O = {}
                K = self.classes[-1]
                
                for w_i in W:
                    
                    O[w_i] = sum([w_j * x(d.features[j]) for j,w_j in enumerate(W[w_i]) ])
                
                for w_i in W:
                    
                    if w_i != K:
                        Y[w_i] = np.exp(O[w_i])/(1 + sum([ np.exp(O[O_i]) if O_i != K else 0 for O_i in O ] ))
                    else:
                        Y[w_i] = 1 - sum([ Y[w_i] if w_i != K else 0 for w_i in Y ])
                
                for w_i in W:
                    
                    for j,w_j in enumerate(W[w_i]):
                        
                        feat = d.features[j] if d.features[j] != 0 else 1
                        d_W[w_i][j] = d_W[w_i][j] + ( r(d.cls,w_i) - Y[w_i] ) * x(d.features[j])
                
            for w_i in W:
                
                for j,w_j in enumerate(W[w_i]):
                    
                    W[w_i][j] = W[w_i][j] + eta * d_W[w_i][j]
        
        self.W = W
        return W
    
    def test(self,data):
        
        total = len(data)
        correct = 0
        
        K = self.classes[-1]
        W = self.W
        
        for d in data:        

            O = {}
            potentials = {}
            probs = []

            for w_i in W:
                O[w_i] = sum([w_j * d.features[j] for j,w_j in enumerate(W[w_i]) ])

            for w_i in W:
                if w_i != K:
                    potentials[w_i] = np.exp(O[w_i])/(1 + sum([ np.exp(O[O_i]) if O_i != K else 0 for O_i in O ] ))
                    probs.append(potentials[w_i])
                else:
                    potentials[w_i] = 1 - sum(probs)
            
            predicted = max(potentials, key=potentials.get)
            actual = d.cls
            
            if actual == predicted:
                correct += 1
            
        return correct / total
    
    def create_one_hot(self,data):
        stats = { }
        for f in self.multi:
            
            for d in data:
                if d.cls not in stats:
                    stats[d.cls] = { }
                if f not in stats[d.cls]:
                    stats[d.cls][f] = {'vals':[]}
                stats[d.cls][f]['vals'].append(d.features[f])
            
        for cls in stats.keys():
            for f in stats[cls]:
                stats[cls][f]['mean'] = np.mean(stats[cls][f]['vals'])
                stats[cls][f]['std'] = np.std(stats[cls][f]['vals'])

        return stats
    
def NBTest(partition,multi_vals):
    accuracies = []
    logging.info('starting cross validation tests')
    for i in range(len(partition)):
        logging.info(f'{i} is the validation set')
        val_set = partition[i]
        train_set = []
        for j,p in enumerate(partition):
            train_set = train_set + p if i != j else train_set
        
        model = NaiveBayesModel(f'{i}',multi_vals)
        model.train(train_set)
        accuracy = model.test(val_set)
        logging.info(f'{i} tested: accuracy={accuracy}')
        accuracies.append(accuracy)
    return np.mean(accuracies)

def LRTest(partition,ds):
    accuracies = []
    for i in range(len(partition)):
        val_set = partition[i]
        train_set = []
        for j,p in enumerate(partition):
            train_set = train_set + p if i != j else train_set
        
        model = LogisticRegressionModel(f'{i}',ds.classes,ds.fields)
        weights = model.train(train_set)
        accuracy = model.test(val_set)
        accuracies.append(accuracy)
    return np.mean(accuracies)